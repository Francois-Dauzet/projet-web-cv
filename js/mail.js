const saisiNom = document.getElementById("nom");
const saisiPrenom = document.getElementById("prenom");
const saisiMail = document.getElementById("email");
const saisiSujet = document.getElementById("sujet");
const saisiMessage = document.getElementById("message");
const successMessageMail = document.getElementById("success-message-mail");
const errorMessageMail = document.getElementById("error-message-mail");
const errorName = document.getElementById("error-name");
const errorFirstName = document.getElementById("error-first-name");
const errorSubject = document.getElementById("error-subject");
const errorMessage = document.getElementById("error-message");
const errorEmail = document.getElementById("error-email");

const form = document.getElementById("form-contact");

const btSubmit = document.querySelector(".btSubmit");

let nomIncorrect = false;
let nomBlur = false;

let prenomIncorrect = false;
let prenomBlur = false;

let sujetIncorrect = false;
let sujetBlur = false;

let messageIncorrect = false;
let messageBlur = false;

let emailIncorrect = false;
let emailBlur = false;

/////////////////////////// name ///////////////////////////////

const checkInputName = (e) => {
  const value = e.target.value;

  if (2 <= value.length) {
    nomIncorrect = false;
    errorName.classList.add("d-none");
  } else {
    nomIncorrect = true;
    errorName.classList.remove("d-none");
  }
};

saisiNom.addEventListener("blur", (e) => {
  nomBlur = true;
  checkInputName(e);
});

saisiNom.addEventListener("input", (e) => {
  if (nomBlur) {
    checkInputName(e);
  }
});

/////////////////////////// firtName ///////////////////////////////

const checkInputFirstName = (e) => {
  const value = e.target.value;

  if (2 <= value.length) {
    prenomIncorrect = false;
    errorFirstName.classList.add("d-none");
  } else {
    prenomIncorrect = true;
    errorFirstName.classList.remove("d-none");
  }
};

saisiPrenom.addEventListener("blur", (e) => {
  prenomBlur = true;
  checkInputFirstName(e);
});

saisiPrenom.addEventListener("input", (e) => {
  if (prenomBlur) {
    checkInputFirstName(e);
  }
});

/////////////////////////// subject ///////////////////////////////

const checkInputSubject = (e) => {
  const value = e.target.value;

  if (5 <= value.length) {
    sujetIncorrect = false;
    errorSubject.classList.add("d-none");
  } else {
    sujetIncorrect = true;
    errorSubject.classList.remove("d-none");
  }
};

saisiSujet.addEventListener("blur", (e) => {
  sujetBlur = true;
  checkInputSubject(e);
});

saisiSujet.addEventListener("input", (e) => {
  if (sujetBlur) {
    checkInputSubject(e);
  }
});

/////////////////////////// message ///////////////////////////////

const checkInputMessage = (e) => {
  const value = e.target.value;

  if (10 <= value.length) {
    messageIncorrect = false;
    errorMessage.classList.add("d-none");
  } else {
    messageIncorrect = true;
    errorMessage.classList.remove("d-none");
  }
};

saisiMessage.addEventListener("blur", (e) => {
  messageBlur = true;
  checkInputMessage(e);
});

saisiMessage.addEventListener("input", (e) => {
  if (messageBlur) {
    checkInputMessage(e);
  }
});

/////////////////////////// mail ///////////////////////////////

const checkInputEmail = (e) => {
  const value = e.target.value;

  if (value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
    emailIncorrect = false;
    errorEmail.classList.add("d-none");
  } else {
    emailIncorrect = true;
    errorEmail.classList.remove("d-none");
  }
};

saisiMail.addEventListener("blur", (e) => {
  emailBlur = true;
  checkInputEmail(e);
});

saisiMail.addEventListener("input", (e) => {
  if (emailBlur) {
    checkInputEmail(e);
  }
});

//////////////////////////////////////////////////////

form.addEventListener("submit", function (event) {
  event.preventDefault();
  //? URL de developpement
  // const url = "http://localhost/cv/php/mail.php";
  //? URL de production
  const url = "php/mail.php";

  const nom = saisiNom.value;
  const prenom = saisiPrenom.value;
  const email = saisiMail.value;
  const sujet = saisiSujet.value;
  const message = saisiMessage.value;

  fetch(url, {
    method: "POST",
    body: JSON.stringify({ nom, prenom, email, sujet, message }),
    Accept: "application/json",
    "Content-Type": "application/json",
  })
    .then((res) => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error("TEST ERROR");
      }
    })
    .then((data) => {
      console.log("SUCCESS");
      console.log(JSON.stringify(data));
      successMessageMail.classList.remove("d-none");
      successMessageMail.innerHTML =
        "Bonjour " + nom + " " + prenom + " votre message a bien été envoyé";
      form.classList.add("d-none");
      errorMessageMail.classList.add("d-none");
    })
    .catch((err) => {
      console.log("ERROR");
      console.log(JSON.stringify(err));
      errorMessageMail.classList.remove("d-none");
    });
});
