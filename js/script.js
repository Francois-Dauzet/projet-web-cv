///////////////////////////////// navBarScroll //////////////////////////////////

$(window).on("scroll", function () {
  if ($(window).scrollTop()) {
    $("nav").addClass("scroll");
  } else {
    $("nav").removeClass("scroll");
  }
});

$(window).on("scroll", function () {
  if ($(window).scrollTop()) {
    $(".btnNavBar").addClass("scrollBtn");
  } else {
    $(".btnNavBar").removeClass("scrollBtn");
  }
});

///////////////////////////////// Formulaires //////////////////////////////////////

const btnValide = document.getElementById("btnValide");
const champs = document.querySelectorAll(".champ");
let borderInput = "";

champs.forEach((champ) => {
  champ.addEventListener("input", function (evt) {
    if (champ.value) {
      champ.style.border = borderInput;
    }
  });
});
btnValide.addEventListener("click", function () {
  champs.forEach((champ) => {
    if (champ.value.trim() == "") {
      borderInput = champ.style.border;
      champ.placeholder = "Champ obligatoire !";
      champ.style.border = "2px solid red";
      champ.style.color = "red";
      champ.style.fontSize = "15px";
    }
  });
});

///////////////////////////////// Animation drop //////////////////////////////////////

// --------------------- Left Animation ---------------------
const optionsLeft = {
  rootMargin: "-5%",
};

const callbackLeft = (entriesLeft) => {
  entriesLeft.forEach((entryLeft) => {
    if (entryLeft.isIntersecting) {
      entryLeft.target.classList.remove("dropAnimLeft");
    } else {
      entryLeft.target.classList.add("dropAnimLeft");
    }
  });
};

const observerLeft = new IntersectionObserver(callbackLeft, optionsLeft);

const dataAnimLeft = [...document.querySelectorAll("[data-dropAnimLeft]")];
dataAnimLeft.forEach((el) => observerLeft.observe(el));

// --------------------- Right Animation ---------------------
const optionsRight = {
  rootMargin: "-5%",
};

const callbackRight = (entriesRight) => {
  entriesRight.forEach((entryRight) => {
    if (entryRight.isIntersecting) {
      entryRight.target.classList.remove("dropAnimRight");
    } else {
      entryRight.target.classList.add("dropAnimRight");
    }
  });
};

const observerRight = new IntersectionObserver(callbackRight, optionsRight);

const dataAnimRight = [...document.querySelectorAll("[data-dropAnimRight]")];
dataAnimRight.forEach((el) => observerRight.observe(el));

// --------------------- Opacity Animation ---------------------
const optionsOpacity = {
  rootMargin: "25%",
};

const callbackOpacity = (entriesOpacity) => {
  entriesOpacity.forEach((entryOpacity) => {
    if (entryOpacity.isIntersecting) {
      entryOpacity.target.classList.remove("dropAnimOpacity");
    } else {
      entryOpacity.target.classList.add("dropAnimOpacity");
    }
  });
};

const observerOpacity = new IntersectionObserver(
  callbackOpacity,
  optionsOpacity
);

const dataAnimOpacity = [
  ...document.querySelectorAll("[data-dropAnimOpacity]"),
];
dataAnimOpacity.forEach((el) => observerOpacity.observe(el));

// --------------------- Rotate Animation ---------------------
const optionsRotate = {
  rootMargin: "-15%",
};

const callbackRotate = (entriesRotate) => {
  entriesRotate.forEach((entryRotate) => {
    if (entryRotate.isIntersecting) {
      entryRotate.target.classList.remove("dropAnimRotate");
    } else {
      entryRotate.target.classList.add("dropAnimRotate");
    }
  });
};

const observerRotate = new IntersectionObserver(callbackRotate, optionsRotate);

const dataAnimRotate = [...document.querySelectorAll("[data-dropAnimRotate]")];
dataAnimRotate.forEach((el) => observerRotate.observe(el));

///////////////////////////////// Btn Nav //////////////////////////////////////

const btnNavBar = document.querySelector("#btnNavBar");
const btnNavBarBack = document.querySelector("#btnNavBarBack");
const menuNav = document.querySelectorAll(".menuNav");

btnNavBar.addEventListener("click", () => {
  document.querySelector(".containerMenuNav").style.display = "flex";
  document.querySelector(".btnNavBar").style.display = "none";
  document.querySelector(".btnNavBarBack").style.display = "block";
  setTimeout(() => {
    document
      .querySelector(".containerMenuNav")
      .classList.remove("hiddenMenuNav");
  }, 100);
});

btnNavBarBack.addEventListener("click", () => {
  document.querySelector(".btnNavBar").style.display = "block";
  document.querySelector(".btnNavBarBack").style.display = "none";
  document.querySelector(".containerMenuNav").classList.add("hiddenMenuNav");
  setTimeout(() => {
    document.querySelector(".containerMenuNav").style.display = "none";
  }, 100);
});

menuNav.forEach((el) => {
  el.addEventListener("click", () => {
    document.querySelector(".containerMenuNav").classList.add("hiddenMenuNav");
    document.querySelector(".btnNavBar").style.display = "block";
    document.querySelector(".btnNavBarBack").style.display = "none";
    setTimeout(() => {
      document.querySelector(".containerMenuNav").style.display = "none";
    }, 300);
  });
});
