<?php

header("Access-Control-Allow-Origin: *");

$json = file_get_contents('php://input');

$data = json_decode($json, true);

(bool) $prenomIncorrect = true;
(bool) $sujetIncorrect = true;
(bool) $nomIncorrect = true;
(bool) $emailIncorrect = true;
(bool) $messageIncorrect = true;

// Prénom //
if (isset($data["prenom"])) :

    if (2 <= strlen(trim($data["prenom"]))) :
        $prenomIncorrect = false;
    endif;

endif;

// Nom //
if (isset($data["nom"])) :

    if (2 <= strlen(trim($data["nom"]))) :
        $nomIncorrect = false;
    endif;

endif;

// Email //
if (isset($data["email"])) :

    if (preg_match('/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/', trim($data["email"]))) :
        $emailIncorrect = false;
    endif;

endif;

// Sujet //
if (isset($data["sujet"])) :

    if (5 <= strlen(trim($data["sujet"]))) :
        $sujetIncorrect = false;
    endif;

endif;

// Message //
if (isset($data["message"])) :

    if (isset($data["message"])) :
        if (10 <= strlen(trim($data["message"]))) :
            $messageIncorrect = false;
        endif;
    endif;

endif;


// Messages d'ERREUR //
if ($prenomIncorrect || $nomIncorrect || $emailIncorrect || $sujetIncorrect || $messageIncorrect) :

    // HTTP_CODE 400 = bad request
    http_response_code(400);
    print(json_encode('Champ incorrect'));

else :

    try {

        $to      = 'francoisdauzet.dev@gmail.com';
        $subject = $data['sujet'];
        $message = "prenom : {$data['prenom']} <br/>
        nom : {$data['nom']}
        email : {$data['email']} <br/>
        message : <br/> {$data['message']}";

        $headers = array(
            'From' => 'francois.DAUZET@labo-ve.fr',
            'Reply-To' => 'francois.DAUZET@labo-ve.fr',
            'Content-type' => 'text/html',
            'X-Mailer' => 'PHP/' . phpversion()
        );

        mail($to, $subject, $message, $headers);

        ///////// Connect BDD /////////
        require 'TBDD.php';
        $db = new TBdd();

        if ($db->Connect(true)) {
            $categorie = $db->insertMessage($data['nom'], $data['prenom'], $data['email'], $data['sujet'], $data['message']);
        } else {
            http_response_code(502);
            print(json_encode("Erreur BDD"));
        }

        // HTTP_CODE 200 = success
        http_response_code(200);
        print(json_encode('Bravo !'));
    } catch (Exception $err) {

        // HTTP_CODE 500 = internal server error
        http_response_code(500);
        print(json_encode("Votre email n'a pas été envoyé. Dommage!!!"));
    }

endif;
