<?php
class TBdd
{
    // Attributs
    public $IsConnected;
    public $ErrCode;
    public $ErrDesc;
    public $Database;

    ////////////////////////////// Connect /////////////////////////////////

    public function Connect(bool $Order): bool
    {

        if ($Order) {
            try {
                require 'config.php';
                $this->Database    = new \PDO(
                    $typeServeurBdd . ':dbname=' . $nomBdd . ';host=' . $adresseServeurBdd,
                    $userName,
                    $userPassword
                );
                return true;
            } catch (PDOException $e) {
                echo $e;
                return false;
            }
        }
    }

    ////////////////////////////// Insert Message /////////////////////////////////

    public function insertMessage(string $name, string $first_name, string $subject, string $email, string $body)
    {
        if ($this->Database != null) {
            try {
                $MyReq   = $this->Database->prepare("INSERT INTO `message` (`name`, `first_name`, `email`, `subject`, `body`) VALUES (:name, :first_name, :email,:subject,:body);");

                $MyReq->bindValue(':name', $name, PDO::PARAM_STR);
                $MyReq->bindValue(':first_name', $first_name, PDO::PARAM_STR);
                $MyReq->bindValue(':email', $email, PDO::PARAM_STR);
                $MyReq->bindValue(':subject', $subject, PDO::PARAM_STR);
                $MyReq->bindValue(':body', $body, PDO::PARAM_STR);

                if ($MyReq->execute()) {
                    $MyArray = $MyReq->fetchAll();
                    return $MyReq->rowCount();
                } else {
                    print 'On a un pb' . PHP_EOL;
                    return false;
                }
            } catch (PDOException $e) {
                var_dump($e);
                return false;
            }
        };
    }
}
